package com.bolsadeideas.springboot.di.app.models.domain;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope 
/*
 * Con esto marcamos que la factura va a durar lo que dure la petición del usuario.
 * Por defecto si no le marcamos nada a la clase, Spring entiende que es un Componente Singleton que solo lo
 * construye una sola vez en toda la aplicación y para todos los clientes y para todas las llamadas es el mismo.
 * Si lo marcamos con @RequestScope ya pasa a ser propio de cada llamada (request) y lo vuelve a construir en cada una de ellas.
 * NOTA: hay que fijarse en todos los atributos que tenga esta clase que puedan tambien necesitar añadirle el @RequestScope
 * por que si no pasaría lo mismo.
 * En caso de que la clase Cliente NO ESTÉ MARCADA con @RequestScope, empezará a concatenar en cada llamada el nombre del
 * cliente y el apellido al String anterior, pues solo habrá uno para el mismo CONTEXTO mostrándolo por la consola y por la pantalla.
 * 
 * 
 */
//@SessionScope
/*
 *Para trabajar con sesiones, para que nuestro objeto sea persistente en la sesión http: un carro de compra por ejemplo
 *Finaliza cuando se invalida la sesión, cuando cerramos el navegador...
 *IMPORTANTÍSIMO: si trabajamos con la SessionScope es necesario MARCAR LA CLASE CON Serializable.
 *Lo mismo pasará cuando queramos transportar una clase con JPA
 */
//@ApplicationScope
public class Factura implements Serializable{

	private static final long serialVersionUID = 946004357128146951L;

	@Value("${factura.descripcion}")
	private String descripcion;
	
	@Autowired
	private Cliente cliente;
	
	@Autowired
	//@Qualifier("itemsFacturaOficina")
	private List<ItemFactura> items;
	
	@PostConstruct //Es muy parecido a un constructor pero permite que Spring por un lado realice la construcción del objeto, y despues nosostros modificamos lo que queramos
	public void inicializar() {
		cliente.setNombre(cliente.getNombre().concat(" ").concat("José"));
		this.descripcion = this.descripcion.concat(" del cliente: ").concat(cliente.getNombre());
	}
	
	@PreDestroy //En un contexto @SessionScope no se ejecuta el PreDestroy porque estamos dentro de una sesión hasta que se cierre la sesión o el navegador
	public void destruir() {
		System.out.println("Factura destruida".concat(descripcion));
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public List<ItemFactura> getItems() {
		return items;
	}
	
	public void setItems(List<ItemFactura> items) {
		this.items = items;
	}
	
}
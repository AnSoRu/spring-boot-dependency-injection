package com.bolsadeideas.springboot.di.app.models.services;

//@Component("miServicioSimple")
//@Primary
public class MiServicio implements IServicio{

	@Override
	public String operacion() {
		return "ejecutando algún proceso importante...simple";
	}
	
	//Una clase @Component siempre tiene que tener un constructor sin argumentos
	public MiServicio() {}
}

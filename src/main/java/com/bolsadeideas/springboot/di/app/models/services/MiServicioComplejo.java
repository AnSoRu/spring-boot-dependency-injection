package com.bolsadeideas.springboot.di.app.models.services;

//@Component("miServicioComplejo")
//@Primary //Sirve para cuando tenemos varias clases que implementan la misma interfaz hay que indicarle a Spring cual tiene que ser
//la clase Principal que tiene que inyectar
public class MiServicioComplejo implements IServicio{

	@Override
	public String operacion() {
		return "ejecutando algún proceso importante...complicado";
	}
	
	//Una clase @Component siempre tiene que tener un constructor sin argumentos
	public MiServicioComplejo() {}
}

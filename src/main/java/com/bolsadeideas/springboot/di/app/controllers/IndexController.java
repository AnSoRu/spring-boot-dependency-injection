package com.bolsadeideas.springboot.di.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.bolsadeideas.springboot.di.app.models.services.IServicio;

@Controller
public class IndexController {
	
	//private MiServicio servicio = new MiServicio();
	
	//@Autowired //Simplemente en lugar de declararnoslo nosotros con new Si lo hemos registrado con un @Component o @Service, el @Autowired lo buscará en todo el proyecto
	//private MiServicio servicio;
	
	//Pero en lugar de utilizar una clase concreta de servicio vamos a utilizar una interfaz
	@Autowired
	@Qualifier("miServicioComplejo")//Para inyectar un servicio concreto
	private IServicio servicio;
	
	//Inyección por constructor, aunque se le puede quitar el autowired
	
	//public IndexController(IServicio servicio) {
		//Y es que cuando en los constructores se les pasa un componente que está manejado con Spring (marcado con @Component)
		//Lo inyecta de manera automática, da igual ponerle o no ponerle el @Autowired
		//this.servicio = servicio;
	//}

	@GetMapping({"/index","/",""})
	public String index(Model model) {
		model.addAttribute("objeto",servicio.operacion());
		return "index";
	}
	
	//Quitamos el @Autowired del atributo privado MiServicio servicio y funciona de la misma manera
	/*@Autowired
	public void setServicio(IServicio servicio) {
		this.servicio = servicio;
	}
	*/
	

}
